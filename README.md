# Magento1_Payment1
This is Magento 1.x module for payment through the Payment Gateway (the user does not enter addition information for this payment method during checkout).

After installing this module in the admin panel, the new menu item will appear in the System -> Configuration -> Payment Methods. Its name is "Medvslav PaymentMethod1 Module".
You can enter the configuration of this payment module here.

The form has following fields:

- Title

- Enabled (for activating this payment method in the checkout process)
- New order status

- Payment from applicable countries

- Payment from Specific countries

After installing this module you have to activate this payment method in the configuration.>

The module uses not a real Payment Gateway for the imitation payment process  (the user won't get charged).
#